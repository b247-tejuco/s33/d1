console.log('helowrd')
// [SECTION] Javascript Synchronous vs Asynchronous
// Javascript by default is synchronous. It reads codes line by line. Only one statement is executed at a time.

// example 1
console.log('helo1')
// conosle.log('helo eror') // an error will occur
console.log('helo again') // did not show in console. because of the error before it. Concludes that javascript is synchronous

// example2

/*for(let i=0; i<=500; i++){
	console.log(i)
}*/
console.log('Hellow agains')


// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.

// the fetch API allows you to asynchronously request for a resources(data)

// Syntax
	// fetch('URL')

console.log(fetch("https://jsonplaceholder.typicode.com/posts"))

// syntax
	// fetch('URL')
	// .then((response)=>{})

	fetch('https://jsonplaceholder.typicode.com/posts')
	.then(response=> console.log(response.status))

// [SECTION] Getting All Post

	// fetch function returns a promise. '.then' function alows for process of that promise in order to extract data from it. It also waits for the promise to be fulfilled before moving forward
	fetch('https://jsonplaceholder.typicode.com/posts')
		// use the json method from the 'response' object to convert the data retrieved into JSON format to be used in our application.
	.then((response)=> response.json())
	// print the converted JSON value from the  'fetch' request
	// 'promise chain' is the use of multiple 'then' methods
	.then((data)=> console.log(data))

		//[SUB SECTION] The Async and Await

		// creates an asynchronous function
		async function fetchData(){

			// waits for the 'fetch' method to complete then stores the value in the 'result' variable
			let result= await fetch('https://jsonplaceholder.typicode.com/posts')
			// Result returned by fetch is = promise returned
			console.log(result)
			console.log(typeof result)

			// we cannot access the content of the "response" by directly accessing its body property. Thats why we convert to jSON next
			console.log(result.body)

			//converts the data from the "Response" object as JSON
			let json= await result.json()
			// print out the content of the "response" object
			console.log(json)
		}
		fetchData()


	// [SECTION] Getting a Specific Post
		// Retrieve a specific post following the REST API (retrieve, /posts/:id, GET)

		fetch('https://jsonplaceholder.typicode.com/posts/99') //fetch
		.then((response)=> response.json()) //then, convert response to json
		.then((data)=> console.log(data)) //then, print the converted data in console


	//[SECTION] Creating a Post
		//Creates a new post following the REST API(create, /posts/:id, POST)
		
		fetch('https://jsonplaceholder.typicode.com/posts', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: 'New Post',
				body: 'Hello World',
				userId: 1
			})
		})
		.then((response)=> response.json())
		.then((data)=> console.log(data))

	// [SECTION] Updating a Post
		// Update a specific post following the REST API (update, /posts/:id, Patch)

		fetch('https://jsonplaceholder.typicode.com/posts/1', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				title: 'Corrected Post!'
			})
		})
		.then((response)=>response.json())
		.then((data)=> console.log(data))

		// difference between PUT and PATCH
			// PUT is used to update single/several properties
			//PATCH is used to update the whole object


		// [SECTION] Deleting a Post
			// Deleting a specific post following the REST API (delete, /posts/:id, DELETE)

			fetch('https://jsonplaceholder.typicode.com/posts/1', {
				method: 'DELETE'
			})


	//[SECTION] Retrieve Comments from a Single Post
			//Retrieve comments for a specific post following the REST API (retrieve, /posts/:id/comments, GET)
			
			fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
			.then((response)=> response.json())
			.then((data)=> console.log(data))